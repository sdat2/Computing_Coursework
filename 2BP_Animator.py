'''
This program is designed to take the text output files of the 1_Body_Problem program,
and plot them to produce an effective animation '''

import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation
import pickle


####Read In Data######
output_direc = '/Users/simon/Documents/Computing_Coursework/2BP_Tests'

with open(output_direc+'/C_grid.pickle', 'rb') as handle:
    coordinate_grid = pickle.load(handle)
with open(output_direc+'/timer.pickle', 'rb') as handle:
    timer = pickle.load(handle)
with open(output_direc+'/TotEnergy.pickle','rb') as handle:
    Total_Energies = pickle.load(handle)

print(len(timer))

dt = timer[1]-timer[0]

###############Plotting############################
# set up figure and animation
fig = plt.figure()
ax = fig.add_subplot(111, aspect='equal', autoscale_on=False,
                     xlim=(-7, 7), ylim=(-7, 7))
ax.grid()
line, = ax.plot([], [], 'bo', markersize=2)
galactic_centre, = ax.plot([0], [0], color='red', marker='o')
impactor, = ax.plot([0], [0], color='green', marker='o')
plt.xlabel(r'X')
plt.ylabel(r'Y')
time_text = ax.text(0.02, 0.95, '', transform=ax.transAxes)
KE_text = ax.text(0.02, 0.90, '', transform=ax.transAxes)
GPE_text = ax.text(0.02, 0.85, '', transform=ax.transAxes)
energy_text = ax.text(0.02, 0.80, '', transform=ax.transAxes)

def init():
    """initialize animation"""
    line.set_data([], [])
    galactic_centre.set_data([], [])
    impactor.set_data([], [])
    time_text.set_text('')
    KE_text.set_text('')
    GPE_text.set_text('')
    energy_text.set_text('')
    return line, galactic_centre, impactor, time_text, KE_text, GPE_text, energy_text

def animate(i):
    """perform animation step"""
    line.set_data(coordinate_grid[0,:,i],coordinate_grid[1,:,i])
    galactic_centre.set_data(coordinate_grid[0,0,i], coordinate_grid[1,0,i])
    impactor.set_data(coordinate_grid[0,1,i], coordinate_grid[1,1,i])
    time_text.set_text('time = %.1f' % timer[i])
    KE_text.set_text('KE = %.2f' % Total_Energies['Kinetic'][i])
    GPE_text.set_text('GPE = %.2f' % Total_Energies['Gravitational'][i])
    energy_text.set_text('energy = %.2f' % Total_Energies['Total'][i])
    return line, galactic_centre, impactor, time_text, KE_text, GPE_text, energy_text

# choose the interval based on dt and the time to animate one step
from time import time
t0 = time()
animate(0)
t1 = time()
interval = 50 * dt - (t1 - t0)

ani = animation.FuncAnimation(fig, animate, frames=len(timer),
                              interval=interval, blit=True, init_func=init)

#plt.show()

ani.save(output_direc + '/Simple_Equal.mp4')
