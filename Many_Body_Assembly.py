'''Many_Body_Assembly - a program by sdat2 to show that the
particle class works to effectively simulate a series of test masses
Usage: python3 1_Body_Problem.py [Time_Period] [Time_Step]'''

import sys, os
import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as plt
import pickle


### Global Variable defaults ###
tstep = 0.05                   # Time step
maxtimer = 20

### Command Line Inputs ###
if len(sys.argv) > 1: maxtimer = int(sys.argv[1])
if len(sys.argv) > 2: tstep = float(sys.argv[2])

### A class for particles gravitationally interacting ###
class particle:
    '''A class for gravitationally interacting particles'''
    def __init__(self, position, velocity, mass, forceful, number):
        self.no = number
        self.x = position
        self.v = velocity
        self.m = mass
        self.forceful = forceful
        self.AM()
        self.kin_energy()
    def X(self, position):
        self.x = position
    def V(self, velocity):
        self.v = velocity
    def AM(self):
        '''Perform simple cross product'''
        self.am = np.array([self.v[2]*self.x[1] - self.x[2]*self.v[1],\
        self.x[2]*self.v[0] - self.v[2]*self.x[0],\
        self.v[1]*self.x[0] - self.x[1]*self.v[0]])
    def kin_energy(self):
        self.ke = 0.5*self.m*(LA.norm(self.v))**2
    def gravp_energy(self, particles):
        '''Sum up gravpot contributions'''
        GM = 1
        gpe_temp = 0
        for part in particles:
            if part.forceful and not part.no == self.no:
                gpe_temp += - GM*part.m/LA.norm(part.x - self.x)
        self.gpe = gpe_temp
    def RK4O(self, particles, tstep=tstep):
        '''Implementation of fourth order Runge Kutta'''
        X1 = self.x
        V1 = self.v
        dx1, dv1 = V1, Force(self.no, X1, particles)
        X2 = X1 + dx1*tstep/2
        V2 = V1 + dv1*tstep/2
        dx2, dv2 = V2, Force(self.no, X2, particles)
        X3 = X1 + dx2*tstep/2
        V3 = V1 + dv2*tstep/2
        dx3, dv3 = V3, Force(self.no, X3, particles)
        X4 = X1 + dx3*tstep
        V4 = V1 + dv3*tstep
        dx4, dv4 = V4, Force(self.no, X4, particles)
        self.X((X1 + (dx1+2*dx2+2*dx3+dx4)*(tstep/6)))
        self.V(V1 + ((dv1+2*dv2+2*dv3+dv4)*(tstep/6)))

def Force(num, position, particles):
    '''Gravitational Force Function'''
    GM = 1
    force = np.array([0.0, 0.0, 0.0])
    for part in particles:
        if part.forceful and not part.no == num:
            force += GM*part.m*(part.x-position)/(LA.norm(part.x - position)**(3))
    return force

###Particle Creation###

def Circular_Position(radius, number_particles, member):
    '''returns the initial position of a particle'''
    angle = 2*np.pi/(number_particles)*member
    position = np.array([np.cos(angle),np.sin(angle),0])*radius
    return position

def Circular_Velocity(radius, number_particles, member):
    '''returns the velocity needed for circular motion'''
    angle = 2*np.pi/(number_particles)*member
    GM = 1
    speed = np.sqrt(GM/radius)
    velocity = np.array([-np.sin(angle),np.cos(angle),0])*speed
    return velocity

def Particle_Creator():
    '''creates the particles'''
    global particles
    particles = []
    radii = [2, 3, 4, 5, 6]
    num_particles = [12, 18, 24, 30, 36]
    countA = 0
    particles.append(particle(np.array([0, 0, 0]), np.array([0, 0, 0]), 1.0, True, countA))

    for i in range(len(radii)):
        countB = 0
        for j in range(num_particles[i]):
            countA += 1
            countB += 1
            pos = Circular_Position(radii[i], num_particles[i], j)
            vel = Circular_Velocity(radii[i], num_particles[i], j)
            particles.append(particle(pos, vel, 1.0, False, countA))

    for i in range(len(particles)): particles[i].gravp_energy(particles)

Particle_Creator()

####Run#######
def Write_Out(particles=particles):
    '''Writes output so that it can be animated'''
    output_direc = '/Users/simon/Documents/Computing_Coursework/MBA_Tests'
    if not os.path.exists(output_direc):
        os.makedirs(output_direc)
    with open(output_direc+'/C_grid.pickle', 'wb') as handle:
        pickle.dump(coordinate_grid, handle, protocol=pickle.HIGHEST_PROTOCOL)
    with open(output_direc+'/timer.pickle', 'wb') as handle:
        pickle.dump(timer, handle, protocol=pickle.HIGHEST_PROTOCOL)
    with open(output_direc+'/TotEnergy.pickle', 'wb') as handle:
        pickle.dump(Total_Energies, handle, protocol=0)

'''
    Coordinates = np.zeros((3, len(particles)))
    for i in range(len(particles)):
        Coordinates[:,i]=particles[i].x
    plt.plot(Coordinates[0,:], Coordinates[1,:], marker='o', color='blue',\
    markersize=2, line=None)
    plt.plot([0], [0], marker='x', color='red')
    plt.grid()
    plt.xlabel(r'x Coordinate')
    plt.ylabel(r'y Coordinate')
    plt.axis('scaled')
    plt.savefig(output_direc+'/InitialConfigCoordinates1.pdf')
    plt.clf()
'''
#Write_Out()
def Tot_energies(particles=particles):
    '''adds up all the energy'''
    Kinetic = 0.0; Potential = 0.0
    for part in particles:
        Kinetic += part.ke
        Potential += part.gpe
    return Kinetic, Potential/2, Kinetic+Potential/2

def Spinner(maxtimer=maxtimer , tstep=tstep):
    '''Runs Program'''
    global timer, coordinate_grid, particles, Total_Energies

    timer = np.linspace(0, maxtimer, num=int(maxtimer/tstep))
    coordinate_grid = np.zeros((3,len(particles),len(timer)))

    Total_Energies = dict()
    Total_Energies['Kinetic'] = np.zeros(len(timer))
    Total_Energies['Gravitational'] = np.zeros(len(timer))
    Total_Energies['Total'] = np.zeros(len(timer))

    for f in range(len(timer)):
        for i in range(1, len(particles)):
            particles[i].RK4O(particles, tstep=tstep)
            coordinate_grid[:, i, f] = particles[i].x
            '''particles[i].AM()'''
            particles[i].kin_energy()
            particles[i].gravp_energy(particles)
        Total_Energies['Kinetic'][f], Total_Energies['Gravitational'][f], Total_Energies['Total'][f] = Tot_energies()

Spinner()
Write_Out()
