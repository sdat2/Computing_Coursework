import numpy as np
from numpy import linalg as LA

### Global Variable defaults ###
tstep = 0.05                   # Time step
maxtimer = 20

### A class for particles gravitationally interacting ###
class particle:
    '''A class for gravitationally interacting particles'''
    def __init__(self, position, velocity, mass, forceful, number):
        self.no = number
        self.x = position
        self.v = velocity
        self.m = mass
        self.forceful = forceful
        self.AM()
        self.kin_energy()
    def X(self, position):
        self.x = position
    def V(self, velocity):
        self.v = velocity
    def AM(self):
        '''Perform simple cross product'''
        self.am = np.array([self.v[2]*self.x[1] - self.x[2]*self.v[1],\
        self.x[2]*self.v[0] - self.v[2]*self.x[0],\
        self.v[1]*self.x[0] - self.x[1]*self.v[0]])
    def kin_energy(self):
        self.ke = 0.5*self.m*(LA.norm(self.v))**2
    def gravp_energy(self, particles):
        '''Sum up gravpot contributions'''
        GM = 1
        gpe_temp = 0
        for part in particles:
            if part.forceful and not part.no == self.no:
                gpe_temp += - GM*self.m*part.m/LA.norm(part.x - self.x)
        self.gpe = gpe_temp
    def RK4O(self, particles, tstep=tstep):
        '''Implementation of fourth order Runge Kutta'''
        X1 = self.x
        V1 = self.v
        dx1, dv1 = V1, Force(self.no, X1, particles)
        X2 = X1 + dx1*tstep/2
        V2 = V1 + dv1*tstep/2
        dx2, dv2 = V2, Force(self.no, X2, particles)
        X3 = X1 + dx2*tstep/2
        V3 = V1 + dv2*tstep/2
        dx3, dv3 = V3, Force(self.no, X3, particles)
        X4 = X1 + dx3*tstep
        V4 = V1 + dv3*tstep
        dx4, dv4 = V4, Force(self.no, X4, particles)
        self.X((X1 + (dx1+2*dx2+2*dx3+dx4)*(tstep/6)))
        self.V(V1 + ((dv1+2*dv2+2*dv3+dv4)*(tstep/6)))

def Force(num, position, particles):
    '''Gravitational Force Function'''
    GM = 1
    force = np.array([0.0, 0.0, 0.0])
    for part in particles:
        if part.forceful and not part.no == num:
            force += GM*part.m*(part.x-position)/(LA.norm(part.x - position)**(3))
    return force

def Circular_Position(radius, number_particles, member):
    '''returns the initial position of a particle'''
    angle = 2*np.pi/(number_particles)*member
    position = np.array([np.cos(angle),np.sin(angle),0])*radius
    return position

def Circular_Velocity(radius, number_particles, member):
    '''returns the velocity needed for circular motion'''
    angle = 2*np.pi/(number_particles)*member
    GM = 1
    speed = np.sqrt(GM/radius)
    velocity = np.array([-np.sin(angle),np.cos(angle),0])*speed
    return velocity

def ZMF_Transform(particles):
    '''Transforms the velocities of all the particles to the ZMF'''
    forceful_count = 0.0
    mass = 0.0
    momentum = np.array([0.0, 0.0, 0.0])
    for part in particles:
        if part.forceful:
            forceful_count+=1
            momentum += part.m*part.v
            mass += part.m
    ZMF_V = momentum/mass
    for part in particles:
        part.V(part.v - ZMF_V)

def Centre_Transform(transform, particles):
    '''Transforms the positions of all the particles to centre the close approach'''
    for part in particles:
        part.X(part.x - transform)

def Tot_energies(particles):
    '''adds up all the energy'''
    Kinetic = 0.0; Potential = 0.0
    for part in particles:
        if part.forceful:
            Kinetic += part.ke
            Potential += part.gpe
    return Kinetic, Potential/2, Kinetic + Potential/2
