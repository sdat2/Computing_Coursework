'''1_Body_Problem.py - a program by sdat2 to build and test
a new class for gravitationally interating particles.
Usage: python3 1_Body_Problem.py [Time_Period] [Time_Step]'''

import sys, os
import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as plt


### Global Variable defaults ###
tstep = 0.05                   # Time step
maxtimer = 200

### Command Line Inputs ###
if len(sys.argv) > 1: maxtimer = int(sys.argv[1])
if len(sys.argv) > 2: tstep = float(sys.argv[2])

### A class for particles gravitationally interacting ###
class particle:
    '''A class for gravitationally interacting particles'''
    def __init__(self, position, velocity, mass, forceful, number):
        self.no = number
        self.x = position
        self.v = velocity
        self.m = mass
        self.forceful = forceful
        self.AM()
        self.kin_energy()
    def X(self, position):
        self.x = position
    def V(self, velocity):
        self.v = velocity
    def AM(self):
        '''Perform simple cross product'''
        self.am = np.array([self.v[2]*self.x[1] - self.x[2]*self.v[1],\
        self.x[2]*self.v[0] - self.v[2]*self.x[0],\
        self.v[1]*self.x[0] - self.x[1]*self.v[0]])
    def kin_energy(self):
        self.ke = 0.5*self.m*(LA.norm(self.v))**2
    def gravp_energy(self, particles):
        '''Sum up gravpot contributions'''
        GM = 1
        gpe_temp = 0
        for part in particles:
            if part.forceful and not part.no == self.no:
                gpe_temp += - GM*part.m/LA.norm(part.x - self.x)
        self.gpe = gpe_temp
    def RK4O(self, particles, tstep=tstep):
        '''Implementation of fourth order Runge Kutta'''
        X1 = self.x
        V1 = self.v
        dx1, dv1 = V1, Force(self.no, X1, particles)
        X2 = X1 + dx1*tstep/2
        V2 = V1 + dv1*tstep/2
        dx2, dv2 = V2, Force(self.no, X2, particles)
        X3 = X1 + dx2*tstep/2
        V3 = V1 + dv2*tstep/2
        dx3, dv3 = V3, Force(self.no, X3, particles)
        X4 = X1 + dx3*tstep
        V4 = V1 + dv3*tstep
        dx4, dv4 = V4, Force(self.no, X4, particles)
        self.X((X1 + (dx1+2*dx2+2*dx3+dx4)*(tstep/6)))
        self.V(V1 + ((dv1+2*dv2+2*dv3+dv4)*(tstep/6)))

def Force(num, position, particles):
    '''Gravitational Force Function'''
    GM = 1
    force = np.array([0.0, 0.0, 0.0])
    for part in particles:
        if part.forceful and not part.no == num:
            force += GM*part.m*(part.x-position)/(LA.norm(part.x - position)**(3))
    return force

###Time creation####
timer = np.linspace(0, maxtimer, num=int(maxtimer/tstep))

###Particle Creation###
particles = []
particles.append(particle(np.array([0, 0, 0]), np.array([0, 0, 0]), 1.0, True, 0))
particles.append(particle(np.array([1, 0, 0]), np.array([0, 1.1, 0]), 1.0, False, 1))
particles[1].gravp_energy(particles)

####Plotting Variables Creation#####
X = [particles[1].x[0]]
Vx = [particles[1].v[0]]
Y = [particles[1].x[1]]
Vy = [particles[1].v[1]]
Ang_Mom = np.zeros((3, len(timer)))
Ang_Mom[:, 0] = particles[1].am
kin_record = [particles[1].ke]
gravpot_record = [particles[1].gpe]
tot_energy = [particles[1].gpe + particles[1].ke]


####Run####
for i in range(len(timer)-1):
    particles[1].RK4O(particles, tstep=tstep)
    X.append(float(particles[1].x[0]))
    Vx.append(float(particles[1].v[0]))
    Y.append(float(particles[1].x[1]))
    Vy.append(float(particles[1].v[1]))
    particles[1].AM()
    particles[1].kin_energy()
    particles[1].gravp_energy(particles)
    Ang_Mom[:, i+1] = particles[1].am
    kin_record.append(particles[1].ke)
    gravpot_record.append(particles[1].gpe)
    tot_energy.append(particles[1].gpe + particles[1].ke)


###Plot###
output_direc = '/Users/simon/Documents/Computing_Coursework/1BD_Tests'
if not os.path.exists(output_direc):
    os.makedirs(output_direc)

with open(output_direc+'/coordinates.txt', 'w') as o:
    for i in range(len(timer)):
        o.write(str(X[i])+'\t'+str(Y[i])+'\t'+str(timer[i])\
        +'\t'+str(kin_record[i])+'\t'+str(gravpot_record[i])\
        +'\t'+str(tot_energy[i])+'\n')
    o.close()

if False:
    plt.plot(timer, tot_energy, label='Total Energy')
    plt.xlabel(r'Time')
    plt.ylabel(r'Total Energy')
    plt.legend()
    plt.savefig(output_direc+'/EnergyConservation.pdf')
    plt.clf()
if False:
    plt.plot(timer, X, label='x coordinate')
    plt.plot(timer, Y, label='y coordinate')
    plt.xlabel(r'Time')
    plt.ylabel(r'Coordinate Value')
    plt.legend()
    plt.savefig(output_direc+'/CoordinatesEllipse2.pdf')
    plt.clf()
if False:
    plt.plot(timer, kin_record, label='KE')
    plt.plot(timer, gravpot_record, label='GPE')
    plt.plot(timer, tot_energy, label='Total Energy')
    plt.xlabel(r'Time')
    plt.ylabel(r'Energy')
    plt.legend()
    plt.savefig(output_direc+'/EnergyEllipse2.pdf')
    plt.clf()
if False:
    plt.plot(timer, Ang_Mom[0, :], label='x AM')
    plt.plot(timer, Ang_Mom[1, :], label='y AM')
    plt.plot(timer, Ang_Mom[2, :], label='z AM')
    plt.xlabel(r'Time')
    plt.ylabel(r'Momentum Value')
    plt.legend()
    plt.savefig(output_direc+'/AMEllipse2.pdf')
    plt.clf()
if False:
    plt.plot(X, Y)
    plt.plot([0], [0], marker='x', color='red')
    plt.xlabel(r'x Coordinate')
    plt.ylabel(r'y Coordinate')
    plt.axis('scaled')
    plt.savefig(output_direc+'/Ellipse2.pdf')
    plt.clf()
