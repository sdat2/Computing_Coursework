'''Many_Body_Assembly - a program by sdat2 to show that the
particle class works to effectively simulate a series of test masses
Usage: python3 1_Body_Problem.py [Time_Period] [Time_Step]'''

import sys, os
import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as plt
import pickle
#import GravParticles as gp
from GravParticles import *

### Global Variable defaults ###
tstep = 0.05                   # Time step
maxtimer = 20

### Command Line Inputs ###
if len(sys.argv) > 1: maxtimer = int(sys.argv[1])
if len(sys.argv) > 2: tstep = float(sys.argv[2])


def Particle_Creator(Test_Masses):
    '''creates the particles'''
    global particles
    particles = []
    radii = [2, 3, 4, 5, 6]
    num_particles = [12, 18, 24, 30, 36]
    countA = 0
    particles.append(particle(np.array([0.0, 0.0, 0.0]), np.array([0.0, 0.0, 0.0]), 1.0, True, 0))
    if Test_Masses:
        for i in range(len(radii)):
            countB = 0
            for j in range(num_particles[i]):
                countA += 1
                countB += 1
                pos = Circular_Position(radii[i], num_particles[i], j)
                vel = Circular_Velocity(radii[i], num_particles[i], j)
                particles.append(particle(pos, vel, 1.0, False, countA))

    #position np.array([2.83970485, -4.43499683, 0.0]) velocity [ 0.04504967, -0.08530768, 0.0 ]
    #position [-21.20275601, 15.16620624, 0.0] velocity [-0.22540852  0.42634153  0.        ]
    for i in range(len(particles)): particles[i].gravp_energy(particles)

def Write_Out():
    '''Writes output so that it can be animated'''
    output_direc = '/Users/simon/Documents/Computing_Coursework/TC_Tests'
    if not os.path.exists(output_direc):
        os.makedirs(output_direc)
    with open(output_direc+'/C_grid.pickle', 'wb') as handle:
        pickle.dump(coordinate_grid, handle, protocol=pickle.HIGHEST_PROTOCOL)
    with open(output_direc+'/timer.pickle', 'wb') as handle:
        pickle.dump(short_timer, handle, protocol=pickle.HIGHEST_PROTOCOL)
    with open(output_direc+'/TotEnergy.pickle', 'wb') as handle:
        pickle.dump(Total_Energies, handle, protocol=pickle.HIGHEST_PROTOCOL)
    plt.plot(short_timer, Total_Energies['Kinetic'], label='KE')
    plt.plot(short_timer, Total_Energies['Gravitational'], label='GPE')
    plt.plot(short_timer, Total_Energies['Total'], label='Total Energy')
    plt.xlabel(r'Time')
    plt.ylabel(r'Energy')
    plt.legend()
    plt.savefig(output_direc+'/Energies_Equal.pdf')
    plt.clf()

def Spinner(maxtimer=maxtimer , tstep=tstep):
    '''Runs Program'''
    ab=5
    global timer, coordinate_grid, particles, Total_Energies, short_timer
    timer = np.linspace(0, maxtimer, num=int(maxtimer/tstep))
    shortened_length=0

    for f in range(len(timer)):
        if f%ab ==0: shortened_length+=1
    coordinate_grid = np.zeros((3,len(particles),shortened_length))
    short_timer=[]

    Total_Energies = dict()
    Total_Energies['Kinetic'] = np.zeros(shortened_length)
    Total_Energies['Gravitational'] = np.zeros(shortened_length)
    Total_Energies['Total'] = np.zeros(shortened_length)

    for f in range(len(timer)):
        if f%ab == 0:
            for i in range(0, len(particles)):
                particles[i].RK4O(particles, tstep=tstep)
                coordinate_grid[:, i, int(f/ab)] = particles[i].x
                '''particles[i].AM()'''
                particles[i].kin_energy()
                particles[i].gravp_energy(particles)
            Total_Energies['Kinetic'][int(f/ab)], Total_Energies['Gravitational'][int(f/ab)], Total_Energies['Total'][int(f/ab)] = Tot_energies(particles)
            short_timer.append(timer[f])
        else:
            for i in range(0, len(particles)):
                particles[i].RK4O(particles, tstep=tstep)
    short_timer = np.array(short_timer)
    print('position',particles[0].x,'velocity',particles[0].v)
    print('position',particles[1].x,'velocity',particles[1].v)
    print(particles[1].v- particles[0].v)

def SpinForward(time=50.0, tstep=tstep):
    '''A function that continues to simulate the galaxy without graphing'''
    for i in range(int(time/tstep)):
        for i in range(0, len(particles)):
            particles[i].RK4O(particles, tstep=tstep)



Particle_Creator(True)
'''
Centre_Transform(-np.array([2.83970485, -4.43499683, 0.0]), particles)
particles.append(particle(np.array([-21.20275601,15.16620624,0.0]), -np.array([-0.2704148, 0.51167933, 0.0]), 0.2, True, len(particles)))
'''
Centre_Transform(np.array([-3.36401257, -0.25234366, 0. ]), particles)
particles.append(particle(np.array([-40.19268514,44.24342396,0.0]), -np.array([-0.2466842,0.48812257,0.]), 0.2, True, len(particles)))
Centre_Transform(np.array([-8.0,8.0,0.0]), particles)
ZMF_Transform(particles)
Spinner()
Write_Out()

'''
Simons-MacBook-Air:Computing_Coursework simon$ python3 Impactor_Tester.py 100
position [-3.36401257 -0.25234366  0.        ] velocity [-0.04224855  0.00194853  0.        ]
position [-40.19268514  44.24342396   0.        ] velocity [-0.28893275  0.4900711   0.        ]
[-0.2466842   0.48812257  0.        ]
'''
