'''
This program is designed to take the text output files of the 1_Body_Problem program,
and plot them to produce an effective animation '''

import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation


####Read In Data######
X = []; Y = []; T = []; KE = []; GPE = []; TE = []
output_direc = '/Users/simon/Documents/Computing_Coursework/1BD_Tests'
with open(output_direc+'/coordinates.txt', 'r') as o:
    for line in o:
        inner_list = [elt.strip() for elt in line.split('\t')]
        X.append(float(inner_list[0]))
        Y.append(float(inner_list[1]))
        T.append(float(inner_list[2]))
        KE.append(float(inner_list[3]))
        GPE.append(float(inner_list[4]))
        TE.append(float(inner_list[5]))

X = np.array(X)
Y = np.array(Y)
T = np.array(T)
TE = np.array(TE)
GPE = np.array(GPE)
KE = np.array(KE)
dt = T[1]-T[0]

print(len(T))

###############Plotting############################
# set up figure and animation
fig = plt.figure()
ax = fig.add_subplot(111, aspect='equal', autoscale_on=False,
                     xlim=(-2, 2), ylim=(-2, 2))
ax.grid()

line, = ax.plot([], [], 'o-', lw=2)
ax.plot([0], [0], color='red', marker='x')
time_text = ax.text(0.02, 0.95, '', transform=ax.transAxes)
KE_text = ax.text(0.02, 0.90, '', transform=ax.transAxes)
GPE_text = ax.text(0.02, 0.85, '', transform=ax.transAxes)
energy_text = ax.text(0.02, 0.80, '', transform=ax.transAxes)

def init():
    """initialize animation"""
    line.set_data([], [])
    time_text.set_text('')
    KE_text.set_text('')
    GPE_text.set_text('')
    energy_text.set_text('')
    return line, time_text, KE_text, GPE_text, energy_text

def animate(i):
    """perform animation step"""
    #global pendulum, dt
    #pendulum.step(dt)
    line.set_data(np.array([X[i]]),np.array([Y[i]]))
    time_text.set_text('time = %.1f' % T[i])
    KE_text.set_text('KE = %.3f J' % KE[i])
    GPE_text.set_text('GPE = %.3f J' % GPE[i])
    energy_text.set_text('energy = %.3f J' % TE[i])
    return line, time_text, KE_text, GPE_text, energy_text

# choose the interval based on dt and the time to animate one step
from time import time
t0 = time()
animate(0)
t1 = time()
interval = 500 * dt - (t1 - t0)

ani = animation.FuncAnimation(fig, animate, frames=4000,
                              interval=interval, blit=True, init_func=init)

plt.show()

ani.save(output_direc + '/SimpleMovie.mp4')
